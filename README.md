# _Hoilungia hongkongensis_ genome #

This is the repository for all genomic data, annotations, and supplements for:

Eitel, M., Francis, W.R., Varoqueaux, F., Daraspe, J., Osigus, H-J., Krebs, S., Vargas, S., Blum, H., Williams, G.A., Schierwater, B., W�rheide, G. (2018) [Comparative genomics and the nature of placozoan species](https://doi.org/10.1371/journal.pbio.2005359). PLoS Biology 16(7) : e2005359.

For questions about this genome project or data usage, please contact: **woerheide@lmu.de**

---

### Instructions for data usage ###
The data on this repo can be cloned with git, downloaded as a `.zip` (in the [downloads tab](https://bitbucket.org/molpalmuc/hoilungia-genome/downloads/)), or individual files can be directly downloaded by navigating through the folders.

To clone the entire repo (appx 1GB):

`git clone https://bitbucket.org/molpalmuc/hoilungia-genome.git`

Most files on this repo are gzipped, which can be decompressed on most Mac and Linux systems with:

`gunzip name_of_file.gz`

Some files are gzipped-Tar-archives that contain multiple files or alignments, which can be decompressed with a single command:

`tar -zxpf name_of_file.tar.gz`

Raw sequencing data can be downloaded from NCBI SRA, for the [DNA reads](https://www.ncbi.nlm.nih.gov/sra/SRX2610999[accn]), [Moleculo reads](https://www.ncbi.nlm.nih.gov/sra/SRX2611093[accn]), and [RNAseq reads](https://www.ncbi.nlm.nih.gov/sra/SRX2611000[accn]).

### Comparisons to *Trichoplax* ###
All comparisons with *Trichoplax adhaerens* were made using our re-annotated version of the [Triad1 scaffolds](http://genome.jgi.doe.gov/Triad1/Triad1.home.html). Our AUGUSTUS annotation (GFF format) can be downloaded [here](https://bitbucket.org/wrf/genome-reannotations/downloads/Trichoplax_scaffolds_JGI_AUGUSTUS_maxtrack2.gff.gz), and the proteins (FASTA) for orthology and synteny can be downloaded [here](https://bitbucket.org/wrf/genome-reannotations/downloads/Trichoplax_scaffolds_JGI_AUGUSTUS_maxtrack2.prots.fasta.gz).

### Formal species description ###
In this publication we decribe a new species of Placozoa, in a new genus: _Hoilungia hongkongensis_

To comply with the requirements by the International Code of Zoological Nomenclature (ICZN) to make this a valid Nomenclatural Act, we have registered both the genus and the species in Zoobank under the following LSID:

[**_Hoilungia hongkongensis_: LSID urn:lsid:zoobank.org:act:2B0765C1-D939-48FD-B573-5B3F381817B7**](http://zoobank.org:8080/NomenclaturalActs/2b0765c1-d939-48fd-b573-5b3f381817b7)

---

# [sequences](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/sequences/) #
### Genome assembly ###
Final 87Mb assembly of 669 contigs is unmasked. Hardmarked (as Ns) or softmarked (lowercase) were masked with repeatmasker. 

Links will connect to the file subpage, click on **View raw** to directly download the file.

* Hhon_final_contigs_hardmasked.fasta.gz
* Hhon_final_contigs_softmasked.fasta.gz
* [Hhon_final_contigs_unmasked.fasta.gz](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/sequences/Hhon_final_contigs_unmasked.fasta.gz)

### AUGUSTUS/BRAKER de novo gene models ###
The AUGUSTUS/BRAKER gene models, using hints from mapped RNAseq. The proteins mostly match those predicted from Stringtie/Transdecoder, and this was the dataset used for most analyses.

* [Hhon_BRAKER1_CDS.fasta.gz](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/sequences/Hhon_BRAKER1_CDS.fasta.gz)
* [Hhon_BRAKER1_proteins.fasta.gz](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/sequences/Hhon_BRAKER1_proteins.fasta.gz)
* Hhon_BRAKER1_unexpressed_CDS.fasta.gz
* Hhon_BRAKER1_unexpressed_proteins.fasta.gz

### StringTie genome-based transcriptome ###

* [Hhon_STRINGTIE_transcripts.fasta.gz](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/sequences/Hhon_STRINGTIE_transcripts.fasta.gz)
* Hhon_STRINGTIE_transdecoder_CDS.fasta.gz
* [Hhon_STRINGTIE_transdecoder_proteins.fasta.gz](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/sequences/Hhon_TRINITY_transdecoder_proteins.fasta.gz)
* Hhon_STRINGTIE_transdecoder_transcripts.fasta.gz

### Trinity de novo assembled transcriptome ###

* Hhon_TRINITY_transcripts.fasta.gz
* Hhon_TRINITY_transdecoder_CDS.fasta.gz
* Hhon_TRINITY_transdecoder_proteins.fasta.gz
* Hhon_TRINITY_transdecoder_transcripts.fasta.gz

# [tracks](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/tracks/) #
### Gene annotations with TransDecoder CDS predictions ###
* Hhon_BRAKER1_CDS.gff3.gz
* Hhon_BRAKER1_genes.gff3.gz
* Hhon_BRAKER1_unexpressed_CDS.gff3.gz
* Hhon_STRINGTIE_transcripts.gff3.gz
* Hhon_STRINGTIE_transdecoder_CDS.gff3.gz
* Hhon_STRINGTIE_transdecoder_transcripts.gff3.gz
* Hhon_tophat2_stringtie.gtf.gz
* Hhon_TRINITY_transcripts.gff3.gz
* Hhon_TRINITY_transdecoder_CDS.gff3.gz
* Hhon_TRINITY_transdecoder_transcripts.gff3.gz
* Hhon_tRNAs.gff3.gz

### PFAM domain predictions ###
* Hhon_STRINGTIE_transdecoder.pfam.gff3.gz

### Allelic variation ###
* Hhon_final_contigs_hardmasked_repeats.gff3.gz
* Hhon_FREEBAYES_DNA_SNPs.vcf.gz
* Hhon_GATK_DNA_indels.vcf.gz
* Hhon_GATK_DNA_SNPs_exons.vcf.gz
* Hhon_GATK_DNA_SNPs_unique_exons.vcf.gz
* Hhon_GATK_DNA_SNPs_unique_gDNA.vcf.gz
* Hhon_GATK_DNA_SNPs.vcf.gz
* Hhon_GATK_RNA_SNPs_exons.vcf.gz
* Hhon_GATK_RNA_SNPs_unique_exons.vcf.gz
* Hhon_GATK_RNA_SNPs_unique_gDNA.vcf.gz
* Hhon_GATK_RNA_SNPs.vcf.gz

SNP counts for exons, introns, and intergenic (Figure S4) generated with [vcfstats.py](https://bitbucket.org/wrf/sequences/src/master/vcfstats.py):

`vcfstats.py -i Hhon_FREEBAYES_DNA_SNPs.vcf.gz -g ../sequences/Hhon_final_contigs_unmasked.fasta -t Hhon_tophat2_stringtie.gtf -H Hhon_FREEBAYES_DNA_SNPs.vcf.stringtie_hist`

# [MCScanX](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/mcscanx/) #
Data output files from [MCScanX](http://chibba.pgml.uga.edu/mcscan2/) (by [Wang et al 2012](https://dx.doi.org/10.1093%2Fnar%2Fgkr1293)) used to generate synteny in Figure 2.

* ta_L10_hh_L100k.collinearity.gz - output of MCScanX
* ta_L10_hh_L100K.blast.gz - tabular blast results (`-outfmt 6`), as all-v-all
* ta_L10_hh_L100K.pos.gz - positions of all genes (*H. hongkongensis* and *T. adhaerens*)
* ta_L10_hh_L100k.collinearity.gff.gz - above data converted into GFF file for use in genome browsers

GFF generated with [mcscan_to_gff.py](https://github.com/wrf/genomeGTFtools/blob/master/misc/mcscan_to_gff.py)

`mcscan_to_gff.py -c ta_L10_hh_L100k.collinearity --short-positions ta_L10_hh_L100K.pos --short-names hoilungia_short_names > ta_L10_hh_L100k.collinearity.gff`

# [single marker genes alignments](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/single_marker_genes_alignments/) #
Marker genes as separate alignments from classes of sponges, classes of cnidarians, all ctenophores, and all placozoans.

* 16S_alignments.tar.gz
* 18S_alignments.tar.gz
* 28S_alignments.tar.gz
* CO1_alignments.tar.gz
* ND1_alignments.tar.gz

# [matrices](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/matrices/) #
Protein supermatrix alignments, Dayhoff recoded set.

* dataset_1_partitions.txt
* dataset_1.phy.gz
* dataset_2_dayhoff-6_recoded.phy.gz
* dataset_2_partitions.txt
* dataset_2.phy.gz

# [mitochondrial genome](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/mitochondrial_genome/) #
Sequence and genbank annotation file of the complete _H. hongkongensis_ mitochondrial genome.

* Hoilungia_hongkongensis_H13_mitochondrial_genome.fasta.gz
* Hoilungia_hongkongensis_H13_mitochondrial_genome.gb

# [ID countings](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/id_countings/) #
Lists of genes that were classified as orthologs, paralogs, etc, for Figure S8.

# [orthologs](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/orthologs/) #
### 2,870 orthologs set ###
Raw CDS, protein sequences and alignments of both full length _H. hongkongensis_ alleles with _T. adhaerens sequences_. 

* 2870_orthologs_set.tar.gz

### 6,554 orthologs set ###
Raw CDS, protein sequences and alignments of _T. adhaerens_ and _H. hongkongensis_ reference proteins. Summary table of distances and dN/dS calculations for the 6,554 orthologs set.

* 6554_orthologs_set.tar.gz
* orthologs_distances_dnds_trichoplax_hoilungia.tab

# [reproductive isolation](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/reproductive_isolation/) #
Output of the haploweb tool. Raw genomic sequences of three ribosomal proteins, as well as aligments and aligments with indels removed.

* conspecifity_matrix_data.tar.gz
* ribosomal_proteins_gDNA_aligned_indels_removed.tar.gz
* ribosomal_proteins_gDNA_aligned.tar.gz
* ribosomal_proteins_gDNA_sequences.tar.gz

# [single protein alignments](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/single_protein_alignments/) #
Separate alignments for 212 (dataset 1) and 194 (dataset 2) genes used for distance calculations and phylogenetic inferences.

* dataset_1.tar.gz
* dataset_2.tar.gz

# [phylogenetic analyses](https://bitbucket.org/molpalmuc/hoilungia-genome/src/master/phylogenetic_analyses/) #
Output of phylogenetic analyses of 194 concatenated genes (dataset 2) using PhyloBayes and RAxML. Phylobayes analyses contain all output files except the `.chain` file.

* PyloBayes_CAT-GTR_dayhoff6_recoded.tar.gz
* PyloBayes_CAT-GTR_protein.tar.gz
* PyloBayes_GTR_protein.tar.gz
* RAxML_GTR_protein.tar.gz
* RAxML_LG_protein.tar.gz
