# supplemental tables #

* S1 Table: Hoilungia hongkongensis genome assembly statistics. H. hongkongensis genome assembly and annotation statistics in comparison to Trichoplax adhaerens. n/a, not available. (XLSX) 
* S2 Table: Summary ofHoilungia hongkongensis SNP counts. Shown are summaries from the genomic and transcriptomic (M153E-2 strain) datasets. SNP, single nucleotide polymorphism. (XLSX)
* S3 Table: Hoilungia hongkongensis read- and transcript-mapping statistics. (XLSX)
* S4 Table: Results of the Hoilungia hongkongensis BUSCO v3 searches. (XLSX)
* S5 Table: Results of collinearity analysis. Collinearity between the 10 largest Trichoplax
adhaerens (labeled “T.a.”) scaffolds and the associated 144 Hoilungia hongkongensis (labeled “H.h.”) supercontigs >100 Kb. (XLSX)
* S6 Table: Samples used to generate the conspecificity matrix. Shown are all isolates, including their origin. The provided isolate ID was used in Fig 3 in addition to the haplotype. AS, aquarium sample. (XLSX)
* S7 Table: Transcriptomic and genomic data resources. Used data for genetic distance calculations and phylogenetic inferences. A Trinity transcriptome assembly has been generated for each species with a given SRA accession number. Otherwise, transcriptomes and/or protein sequences from genome annotations were used from the reference. SRA, Sequence Read Archive. (XLSX)
* S8 Table: List of OTUs. Listed are all OTUs used for phylogenetic analyses (dataset 2, as speci- fied). The first species ofeach participated most. OTU, operational taxonomic unit. (XLSX)
* S9 Table: Datasets used for distance calculation and phylogenetic inferences. Summary of protein datasets used for distance calculations (dataset 1) and phylogenetic inferences (dataset 2). Matrix length is 37,838 amino acid characters (dataset 1) and 35,799 (dataset 2), respectively. (XLSX)
* S10 Table: Genetic distances (%) between the two placozoan species in comparison to Cni- daria. Shown are averages of all mean genetic distances (±SD) between genera within families and families within orders for the Cnidaria, as well as genetic distances between Hoilungia hongkongensis (labeled “H. h.”) and Trichoplax adhaerens (labeled “T. a.”). The number of all mean pairwise distances used to calculate the average is given in parentheses. (XLSX)
* S11 Table: Accession numbers of mitochondrial genomes used. (XLSX)
