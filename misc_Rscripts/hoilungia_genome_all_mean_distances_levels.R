nucinfile = "~/genomes/hoilungia_hongkongensis/boxplot/1_cannon212_distances_levels.tab"
a18Sinfile = "~/genomes/hoilungia_hongkongensis/boxplot/2_18S_distances_levels.tab"
a28Sinfile = "~/genomes/hoilungia_hongkongensis/boxplot/3_28S_distances_levels.tab"
a16Sinfile = "~/genomes/hoilungia_hongkongensis/boxplot/4_16S_distances_levels.tab"
co1infile = "~/genomes/hoilungia_hongkongensis/boxplot/5_CO1_distances_levels.tab"
nd1infile = "~/genomes/hoilungia_hongkongensis/boxplot/6_ND1_distances_levels.tab"

nucdata = read.table(nucinfile, header=TRUE, sep="\t")
a18Sdata = read.table(a18Sinfile, header=TRUE, sep="\t")
a28Sdata = read.table(a28Sinfile, header=TRUE, sep="\t")
a16Sdata = read.table(a16Sinfile, header=TRUE, sep="\t")
co1data = read.table(co1infile, header=TRUE, sep="\t")
nd1data = read.table(nd1infile, header=TRUE, sep="\t")

levels = c("class","order","family","placozoa")
genenames = c("Nuclear", "18S", "28S", "16S", "CO1", "ND1")

colorset = c(rep(c("#15bfff", "#ff3f6a", "#71e56b"),3),"#8A2BE2")

pdf(file="~/genomes/hoilungia_hongkongensis/boxplot/all_mean_distance_plot.pdf", width=8, height=6)

par(mar=c(4,4.5,1,1))

posvec = c(1,2,3,5,6,7,9,10,11,14)

plot(c(0,14), c(0,0.6), type='n', ylim=c(0,0.6), col=colorset, frame.plot=TRUE, axes=FALSE, pch=15, xlab="", ylab="", cex.lab=1.5, cex=1.5)
axis(1,at=c(2,6,10,14),labels=c("between orders\nwithin classes","between families\nwithin orders","between genera\nwithin families", "Placozoa"), par(mgp=c(4,2,0)), tick=TRUE, cex.axis=1.2)
axis(2,at=seq(0,0.6,0.1), labels=seq(0.0,0.6,0.1), par(mgp=c(2,1,0)), cex.axis=1.2)
title(ylab="Mean uncorrected distance",line=3, cex.lab=1.2)
pchvector = c(15,19,17,18,6,8)

counter = 1

for (levdata in list(nucdata,a18Sdata,a28Sdata,a16Sdata,co1data,nd1data) ) {
	print(levdata)
	phylanames = unique(levdata[,1])
	phylaindex = match(levdata[,1],phylanames)
	distvalues = levdata[,3]
	barposition = match(levdata[,2],levels) * 3 - 3 + match(levdata[,1],phylanames)
	datagroups = split(levdata[,3], barposition)
	datameans = lapply(datagroups,mean)
	points(posvec, datameans, col=colorset, cex=1.5, pch=pchvector[counter] )
	counter = counter + 1
}
#points(c(14),distvalues[which(phylaindex==4)])
legend(10.5,0.6,legend=phylanames[1:3], col=c("#15bfff", "#ff3f6a", "#71e56b"), pch=15, cex=1.4, bty='n') #, fill=TRUE, border="black")
#legend(11,0.72,legend=phylanames[1:3], col="black", pch=0, cex=1.5, bty='n')

legend(10.5,0.45,legend=genenames, col="black", pch=pchvector, cex=1.4, bty='n')

dev.off()





